require('./tariff.scss');
const $ = require('jquery');

$('.tariff__desc-open-btn').on('click', function() {
    let active = false;
    if ($(this).hasClass('tariff__desc-open-btn_active')) {
        active = true;
    }

    $('.tariff__desc-open-btn_active')
        .removeClass('tariff__desc-open-btn_active')
        .siblings('.tariff__description')
        .removeClass('tariff__description_active');

    if (!active) {
        $(this).addClass('tariff__desc-open-btn_active')
            .siblings('.tariff__description')
            .addClass('tariff__description_active');
    }
});