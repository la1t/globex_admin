require('./notification.scss');
require('./notification.svg');

const $ = require('jquery');

$('.notification__btn-close').on('click', function() {
    let target = $(this);
    target.parent().addClass('notification_was-close');
    setTimeout(function() {
        target.parent().remove();
    }, 350);
});