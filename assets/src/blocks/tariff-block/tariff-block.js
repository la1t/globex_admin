require('./tariff-block.scss');

const $ = require('jquery');
require('slick-carousel');
require('slick-carousel/slick/slick.scss');

$('.tariff-block__list').slick({
    infinite: false,
    slidesToShow: 3,
    slidesToScroll: 3,
    vertical: false,
    arrows: true,
    nextArrow: '.paginator__arrow_right',
    prevArrow: '.paginator__arrow_left',
    dots: true,
    dotsClass: 'paginator__list',
    appendDots: $('.paginator')
});