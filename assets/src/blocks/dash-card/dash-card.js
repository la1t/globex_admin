require('./dash-card.scss');
require('./favorite.svg');
require('./credit-card.svg');
require('./print.svg');
require('./id-card.svg');
require('./presentation.svg');
require('./file.svg');
require('./conference.svg');
require('./information.svg');
const $ = require('jquery');



function resizeCards() {
    let maxHeight = 0;
    $('.dash-card').each(function() {
        $(this).attr('style', '');
    }).each(function() {
        if ($(this).height() > maxHeight) {
            maxHeight = $(this).height();
        }
    }).each(function() {
        $(this).height(maxHeight);
    });
}

resizeCards();

$(window).on('resize', resizeCards);