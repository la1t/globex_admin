require('./content.scss');

const $ = require('jquery');
$('.content').on('article:open', function() {
    $(this).addClass('content_article-opened');
}).on('article:close', function() {
    $(this).removeClass('content_article-opened');
});