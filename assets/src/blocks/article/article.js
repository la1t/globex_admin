require('./article.scss');
const $ = require('jquery');

$('.article').on('article:open', function() {
    $(this).addClass('article_active');
}).on('article:close', function() {
    $(this).removeClass('article_active');
});

$('.article__btn-close').on('click', function() {
    $(this).parents('.article').trigger('article:close');
});