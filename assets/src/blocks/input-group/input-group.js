require('./input-group.scss');
const $ = require('jquery');

$('.input-group__input')
    .on('focus', function() {
        $(this).parents('.input-group').addClass('input-group_focus')
    })
    .on('focusout', function() {
        $(this).parents('.input-group').removeClass('input-group_focus')
    })
    .on('change', function() {
        if ($(this).val()) {
            $(this).parents('.input-group').addClass('input-group_has-text');
        } else {
            $(this).parents('.input-group').removeClass('input-group_has-text');
        }
    });