'use strict';

require('./dropdown.scss');
const $ = require('jquery');

$('.dropdown__switch').on('click', function(e) {
    $(this).siblings('.dropdown__menu').trigger('dropdown:open');
    e.stopPropagation();
});

$('.dropdown__menu').on('dropdown:open', function() {
    $(this).toggleClass('dropdown__menu_active');
}).on('dropdown:close', function() {
    $(this).removeClass('dropdown__menu_active');
}).on('click', function(e) {
    console.log('ok');
    e.stopPropagation();
});