require('./tabs.scss');
const $ = require('jquery');

$('.tabs__item').on('tabs:toggle', function() {
    $(this).siblings('.tabs__item_active').removeClass('tabs__item_active');
    $(this).addClass('tabs__item_active');
});