require('./tabs-toggles.scss');
const $ = require('jquery');

$('.tabs-toggles__item').on('click', function() {
    $(this).siblings('.tabs-toggles__item_active').removeClass('tabs-toggles__item_active');
    $(this).addClass('tabs-toggles__item_active');
    $('#'+ $(this).attr('data-tabs-target')).trigger('tabs:toggle');
});