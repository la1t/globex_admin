'use strict';

require('./nav-main.scss');
const $ = require('jquery');

$('.nav-main__burger').on('click', function() {
    $(this).parents('.nav-main').toggleClass('nav-main_inactive');
    if (getCookie('menuState') === 'minimized') {
        document.cookie = 'menuState=opened';
    } else {
        document.cookie = 'menuState=minimized';
    }
});

function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

if (getCookie('menuState') === 'minimized') {
    $('.nav-main__container').css('transition', 'none');
    $('.nav-main').toggleClass('nav-main_inactive');
    setTimeout(function() {
        $('.nav-main__container').attr('style', '');
    }, 100);
}